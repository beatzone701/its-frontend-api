﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Controllers
{
    public class PlaylistDTO
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string SignType { get; set; }
        public FrameDTO[] Frame { get; set; }
    }
}
