﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class ChangeDisplayResponseDTO
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
