﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Controllers
{
    public class ElementDTO
    {
        public string ContentType { get; set; }
        public int TextSize { get; set; }
        public string TextColor { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public int ElementWidth { get; set; }
        public int ElementHeight { get; set; }
        public int StartPointX { get; set; }
        public int StartPointY { get; set; }
        public int FloatingLevel { get; set; }
        public bool IsEditable { get; set; }
        public string Reference { get; set; }
    }
}
