﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class CCTVImageDTO
    {
        public string CCTVImage { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
