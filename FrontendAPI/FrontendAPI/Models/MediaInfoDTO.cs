﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class MediaInfoDTO
    {
        public int Id { get; set; }
        public DateTime? Created { get; set; }
        public int ModifiedBy { get; set; }
        public string MediaName { get; set; }
        public string MediaDescription { get; set; }
        public int MediaPlaceholderType { get; set; }
        public int MediaMeaningType { get; set; }
        public int MediaAspectType { get; set; }
        public int MediaAffairType { get; set; }
        public int IsPermanent { get; set; }
        public string Image { get; set; }
    }
}
