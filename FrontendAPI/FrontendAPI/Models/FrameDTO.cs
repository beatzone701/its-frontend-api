﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Controllers
{
    public class FrameDTO
    {
        public int DelayTime { get; set; }
        public string BackgroundColor { get; set; }
        public ElementDTO[] Element { get; set; }
    }
}
