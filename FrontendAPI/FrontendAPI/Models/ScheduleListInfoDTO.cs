﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class ScheduleListInfoDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public int ModifiedBy { get; set; }
        public ScheduleDateDTO ScheduleDate { get; set; }
    }
}
