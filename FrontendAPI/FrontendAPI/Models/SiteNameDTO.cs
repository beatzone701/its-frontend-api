﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class SiteNameDTO
    {
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public string Description { get; set; }
    }
}
