﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class NotificationListDTO
    {
        public SiteNameDTO SiteName { get; set; }
        public NotificationDTO[] Notification { get; set; }
    }
}
