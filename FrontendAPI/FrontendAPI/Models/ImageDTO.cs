﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class ImageDTO
    {
        public int Id { get; set; }
        public string Extension { get; set; }
        public string Image { get; set; }
        public string MediaName { get; set; }
        public string MediaDescription { get; set; }
        public string MediaPlaceholderType { get; set; }
        public string MediaAspectType { get; set; }
        public string MediaMeaningType { get; set; }
        public string IsPermanent { get; set; }
        public string AfairType { get; set; }
    }
}
