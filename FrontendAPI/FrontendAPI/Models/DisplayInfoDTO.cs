﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class DisplayInfoDTO
    {
        public int SignId { get; set; }
        public string SignName { get; set; }
        public string SignType { get; set; }
        public ImageDTO Media { get; set; }
        public string Mode { get; set; }
    }
}
