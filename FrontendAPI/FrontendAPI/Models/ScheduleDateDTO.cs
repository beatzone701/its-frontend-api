﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class ScheduleDateDTO
    {
        public int Id { get; set; }
        public int NodeId { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public int ScheduleListInfoId { get; set; }
        public DateTime? Created { get; set; }
        public ScheduleListDTO[] ScheduleList { get; set; }
    }
}
