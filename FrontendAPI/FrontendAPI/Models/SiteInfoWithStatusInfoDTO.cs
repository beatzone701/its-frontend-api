﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class SiteInfoWithStatusInfoDTO
    {
        public SiteNameDTO SiteInfo { get; set; }
        public int ConnectionStatus { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public DisplayInfoDTO[] DisplayInfo { get; set; }
        public CCTVImageDTO[] CCTVImages { get; set; }
        public string SchematicImage { get; set; }
        public int Speed { get; set; }
        public int Flow { get; set; }
        public int Density { get; set; }
    }
}
