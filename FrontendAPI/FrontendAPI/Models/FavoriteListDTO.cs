﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class FavoriteListDTO
    {
        public int Id { get; set; }
        public int FavoriteInfoId { get; set; }
        public string SignType { get; set; }
        public int SignOrder { get; set; }
        public int MediaId { get; set; }
        public ImageDTO Image { get; set; } // For display only
        public string PlayingMode { get; set; }
    }
}
