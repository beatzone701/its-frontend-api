﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI.Models
{
    public class FavoriteInfoDTO
    {
        public int Id { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public string FavoriteName { get; set; }
        public FavoriteListDTO[] Favorite { get; set; }
    }
}
