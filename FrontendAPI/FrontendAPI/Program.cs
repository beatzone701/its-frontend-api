﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI
{
    class Program
    {
        static bool isDebugMode = false;
        static string serverProtocol = "http";
        static string serverEndPoint = "*";
        public static int serverPort = 8091;

        static void Main(string[] args)
        {
            // Server base address
            if (isDebugMode)
            {
                serverEndPoint = "localhost";
                serverPort = 8080;
            }
            string baseAddress = string.Format("{0}://{1}:{2}/", serverProtocol, serverEndPoint, serverPort);

            // Run server
            Console.Title = "Frontend API : " + serverPort.ToString();
            try
            {
                // Start server
                WebApp.Start<Startup>(baseAddress);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
                Environment.Exit(0);
            }
            Console.ReadLine();
        }
    }
}
