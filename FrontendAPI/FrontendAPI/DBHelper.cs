﻿using FrontendAPI.Controllers;
using FrontendAPI.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontendAPI
{
    class DBHelper
    {
        private MySqlConnection mDBConnection;
        private List<MySqlCommand> sqliteInsertCommand;

        // Local server
        /*private String server = "localhost";
        private String database = "lms";
        private String userId = "its-admin";
        private String password = "8Eb1soW4Nu4u";*/

        // Remote to server
        /*private String server = "203.113.114.4:3306";
        private String database = "lms";
        private String userId = "its-admin";
        private String password = "8Eb1soW4Nu4u";*/

        // Dev PC
        private String server = "localhost";
        private String database = "lms";
        private String userId = "root";
        private String password = "";

        public DBHelper()
        {
            String strConnection = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + userId + ";" + "PASSWORD=" + password + ";CharSet=utf8;";
            mDBConnection = new MySqlConnection(strConnection);
            sqliteInsertCommand = new List<MySqlCommand>();
        }

        private void openDatabaseConnection()
        {
            try
            {
                if (mDBConnection.State == ConnectionState.Open)
                    mDBConnection.Close();
                mDBConnection.Open();
            }
            catch (Exception ex)
            {
            }
        }

        private void closeDatabaseConnection()
        {
            if (mDBConnection.State == ConnectionState.Open)
            {
                mDBConnection.Close();
            }
        }

        public bool HasImageData(int id)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from media_info where id='" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    closeDatabaseConnection();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public bool HasSign(int id)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from sign_info where sign_id='" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    closeDatabaseConnection();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public int[] GetMediaId(int mediaType, int affairType, int skip, int take)
        {
            try
            {
                List<int> mediaId = new List<int>();
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select id from media_info ";

                if (mediaType != 0)
                {
                    sqliteCommand.CommandText += "where media_placeholder_type = '" + mediaType + "' ";
                }

                if (affairType != 0)
                {
                    if (mediaType != 0)
                    {
                        sqliteCommand.CommandText += "and affair_type = '" + affairType + "' ";
                    }
                    else
                    {
                        sqliteCommand.CommandText += "where affair_type = '" + affairType + "' ";
                    }
                }

                sqliteCommand.CommandText += "order by id asc limit " + skip + ", " + take + ";";
                //Console.WriteLine(sqliteCommand.CommandText);
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("id"))
                        {
                            mediaId.Add(Convert.ToInt16(dataReader.GetValue(i)));
                        }
                    }
                }
                closeDatabaseConnection();
                return mediaId.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public bool ChangeSignDisplay(int signid, int imageid)
        {
            try
            {
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "update playlist_info set media_id='" + imageid + "' where sign_id='" + signid + "';";
                if (HasImageData(imageid) && HasSign(signid))
                {
                    openDatabaseConnection();
                    sqliteCommand.ExecuteNonQuery();
                    closeDatabaseConnection();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public SiteNameDTO[] GetAllSiteInfo()
        {
            try
            {
                List<SiteNameDTO> SiteNameList = new List<SiteNameDTO>();
                SiteNameDTO data = new SiteNameDTO();
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select node_id, node_name, node_description from node_info order by node_id asc;";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    data = new SiteNameDTO();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {

                        if (dataReader.GetName(i).Equals("node_id"))
                        {
                            data.SiteID = (int)dataReader.GetValue(i);
                        }
                        if (dataReader.GetName(i).Equals("node_name"))
                        {
                            data.SiteName = dataReader.GetValue(i).ToString();
                        }
                        if (dataReader.GetName(i).Equals("node_description"))
                        {
                            data.Description = dataReader.GetValue(i).ToString();
                        }
                    }
                    SiteNameList.Add(data);
                }
                closeDatabaseConnection();
                return SiteNameList.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public DisplayInfoDTO[] GetDisplayInfoBySiteId(int id)
        {
            List<DisplayInfoDTO> DisplayInfoList = new List<DisplayInfoDTO>();
            DisplayInfoDTO data;

            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select sign_id, media_id, playing_mode from playlist_info where node_id = '" + id + "' order by sign_id asc;";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                ImageGeneratorController imageGenerator = new ImageGeneratorController();

                while (dataReader.Read())
                {
                    data = new DisplayInfoDTO();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {

                        if (dataReader.GetName(i).Equals("sign_id"))
                        {
                            data.SignId = Convert.ToInt16(dataReader.GetValue(i));
                        }
                        if (dataReader.GetName(i).Equals("sign_name"))
                        {
                            data.SignName = dataReader.GetValue(i).ToString();
                        }
                        if (dataReader.GetName(i).Equals("media_id"))
                        {
                            data.Media = imageGenerator.getImage(Convert.ToInt16(dataReader.GetValue(i)));
                        }
                        if (dataReader.GetName(i).Equals("playing_mode"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    data.Mode = "Auto";
                                    break;
                                case 2:
                                    data.Mode = "Manual";
                                    break;
                                case 3:
                                    data.Mode = "Interrupt";
                                    break;
                                default:
                                    data.Mode = "None";
                                    break;
                            }
                        }
                    }
                    DisplayInfoList.Add(data);
                }
                closeDatabaseConnection();

                DisplayInfoDTO[] returnData = DisplayInfoList.ToArray();

                for (int i = 0; i < returnData.Count(); i++)
                {
                    returnData[i].SignName = GetSignNane(returnData[i].SignId + "");
                    returnData[i].SignType = GetSignType(returnData[i].SignId + "");
                }

                return DisplayInfoList.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return null;
        }

        public string GetSignNane(string id)
        {
            string signName = "";
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select sign_name from sign_info where sign_id = '" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("sign_name"))
                        {
                            signName = dataReader.GetValue(i).ToString();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return signName;
        }

        public string GetSignType(string id)
        {
            string signType = "";
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select sign_type from sign_info where sign_id = '" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("sign_type"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    signType = "MS";
                                    break;
                                case 2:
                                    signType = "VMS";
                                    break;
                                default:
                                    signType = "None";
                                    break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return signType;
        }

        public SiteNameDTO GetSiteInfoById(int id)
        {
            try
            {
                SiteNameDTO data = new SiteNameDTO();
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select node_id, node_name, node_description from node_info where node_id = '" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    data = new SiteNameDTO();
                    dataReader.Read();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {

                        if (dataReader.GetName(i).Equals("node_id"))
                        {
                            data.SiteID = (int)dataReader.GetValue(i);
                        }
                        if (dataReader.GetName(i).Equals("node_name"))
                        {
                            data.SiteName = dataReader.GetValue(i).ToString();
                        }
                        if (dataReader.GetName(i).Equals("node_description"))
                        {
                            data.Description = dataReader.GetValue(i).ToString();
                        }
                    }
                }
                closeDatabaseConnection();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public SiteInfoWithStatusInfoDTO GetSiteInfoWithStatusInfoBySiteId(int id)
        {
            SiteInfoWithStatusInfoDTO SiteInfoWithStatusInfo = null;
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select node_connection_status, node_lat, node_lng, cctv_image, cctv_image_last_update, schematic_image, current_speed, flow, density from node_info where node_id = '" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                SiteInfoWithStatusInfoDTO data;
                if (dataReader.HasRows)
                {
                    List<CCTVImageDTO> CCTVImageList = new List<CCTVImageDTO>();
                    CCTVImageDTO CCTVImage = new CCTVImageDTO();
                    data = new SiteInfoWithStatusInfoDTO();
                    dataReader.Read();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {

                        if (dataReader.GetName(i).Equals("node_connection_status"))
                        {
                            data.ConnectionStatus = Convert.ToInt16(dataReader.GetValue(i));
                        }
                        if (dataReader.GetName(i).Equals("node_lat"))
                        {
                            data.Lat = Convert.ToDouble(dataReader.GetValue(i));
                        }
                        if (dataReader.GetName(i).Equals("node_lng"))
                        {
                            data.Lng = Convert.ToDouble(dataReader.GetValue(i));
                        }
                        if (dataReader.GetName(i).Equals("cctv_image"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                byte[] temp = (byte[])dataReader.GetValue(i);
                                CCTVImage.CCTVImage = Convert.ToBase64String(temp);
                            }
                            else
                            {
                                CCTVImage.CCTVImage = null;
                            }
                        }
                        if (dataReader.GetName(i).Equals("cctv_image_last_update"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                                CCTVImage.LastUpdate = (DateTime)(dataReader.GetValue(i));
                            else
                                CCTVImage.LastUpdate = null;
                        }
                        if (dataReader.GetName(i).Equals("schematic_image"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                byte[] temp = (byte[])dataReader.GetValue(i);
                                data.SchematicImage = Convert.ToBase64String(temp);
                            }
                            else
                            {
                                data.SchematicImage = null;
                            }
                        }
                        if (dataReader.GetName(i).Equals("current_speed"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                data.Speed = Convert.ToInt32(dataReader.GetValue(i));
                            }

                        }
                        if (dataReader.GetName(i).Equals("flow"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                data.Flow = Convert.ToInt32(dataReader.GetValue(i));
                            }

                        }
                        if (dataReader.GetName(i).Equals("density"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                data.Density = Convert.ToInt32(dataReader.GetValue(i));
                            }

                        }
                    }
                    if (CCTVImage != null)
                    {
                        CCTVImageList.Add(CCTVImage);
                        CCTVImage = null;
                    }
                    data.CCTVImages = CCTVImageList.ToArray();
                    SiteInfoWithStatusInfo = data;
                    SiteInfoWithStatusInfo.SiteInfo = GetSiteInfoById(id);
                    SiteInfoWithStatusInfo.DisplayInfo = GetDisplayInfoBySiteId(id);
                }
                closeDatabaseConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return SiteInfoWithStatusInfo;
        }

        public void InsertNotification(string nodeId, string tierId, string systemId, string evenId, string name, string description)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "insert into notification_log (created, node_id, tier_id, system_id, event_id, name, description) values (@date," + nodeId + ", " + tierId + ", " + systemId + ", " + evenId + ", " + name + ", " + description + ")";
                MySqlParameter parameter = new MySqlParameter("@date", MySqlDbType.DateTime);
                parameter.Value = DateTime.Now;
                sqliteCommand.Parameters.Add(parameter);
                sqliteCommand.ExecuteNonQuery();
                closeDatabaseConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public string RetrieveImageData(int id)
        {
            string data = "";
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from media_info where id='" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("media_data"))
                        {
                            data = dataReader.GetValue(i).ToString();
                        }
                    }
                }
                closeDatabaseConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return data;
        }

        public bool HasNoti(string id)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from notification_log where node_id='" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    closeDatabaseConnection();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public NotificationDTO[] GetNotification(string id, int top)
        {
            try
            {
                List<NotificationDTO> data = new List<NotificationDTO>();
                NotificationDTO tmp;
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from notification_log where node_id=\"" + id + "\" and is_closed = 0 order by id asc LIMIT " + top + ";";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    tmp = new NotificationDTO();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("name"))
                        {
                            tmp.Name = dataReader.GetValue(i).ToString();
                        }

                        if (dataReader.GetName(i).Equals("description"))
                        {
                            tmp.Description = dataReader.GetValue(i).ToString();
                        }

                        if (dataReader.GetName(i).Equals("created"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                tmp.Created = (DateTime)(dataReader.GetValue(i));
                            }

                        }

                        if (dataReader.GetName(i).Equals("id"))
                        {
                            tmp.Id = Convert.ToInt16(dataReader.GetValue(i));
                        }
                    }
                    data.Add(tmp);
                }
                closeDatabaseConnection();
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public bool AddToFavorite(FavoriteInfoDTO favorite)
        {
            if (favorite != null)
            {
                try
                {
                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    MySqlTransaction transaction = mDBConnection.BeginTransaction();
                    sqliteCommand.CommandText = "insert into favorite_info (created, favorite_name) values (@date, \"" + favorite.FavoriteName + "\");";
                    MySqlParameter parameter = new MySqlParameter("@date", MySqlDbType.DateTime);
                    parameter.Value = DateTime.Now;
                    sqliteCommand.Parameters.Add(parameter);
                    sqliteCommand.ExecuteNonQuery();
                    sqliteCommand.CommandText = "select id, modified from favorite_info order by modified desc;";
                    MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                    dataReader.Read();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("id"))
                        {
                            favorite.Id = (int)dataReader.GetValue(i);
                        }
                    }
                    dataReader.Close();
                    foreach (FavoriteListDTO favoriteList in favorite.Favorite)
                    {
                        sqliteCommand.CommandText = "insert into favorite_list (favorite_info_id, sign_type, sign_order, media_id, playing_mode) values (\"" + favorite.Id + "\", \"" + favoriteList.SignType + "\", \"" + favoriteList.SignOrder + "\", \"" + favoriteList.MediaId + "\", \"" + favoriteList.PlayingMode + "\");";
                        sqliteCommand.ExecuteNonQuery();
                    }
                    transaction.Commit();
                    closeDatabaseConnection();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return false;
        }

        public bool EditFavorite(FavoriteInfoDTO favorite)
        {
            if (favorite != null)
            {
                try
                {
                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    MySqlTransaction transaction = mDBConnection.BeginTransaction();
                    sqliteCommand.CommandText = "update favorite_info set favorite_name=\"" + favorite.FavoriteName + "\" where id=\"" + favorite.Id + "\";";
                    sqliteCommand.ExecuteNonQuery();
                    foreach (FavoriteListDTO favoriteList in favorite.Favorite)
                    {
                        sqliteCommand = mDBConnection.CreateCommand();
                        if (HasFavoriteList(favoriteList.Id))
                        {
                            sqliteCommand = mDBConnection.CreateCommand();
                            sqliteCommand.CommandText = "update favorite_list set sign_type='" + favoriteList.SignType + "', sign_order='" + favoriteList.SignOrder + "', media_id='" + favoriteList.MediaId + "', playing_mode='" + favoriteList.PlayingMode + "' where id='" + favoriteList.Id + "';";
                            sqliteCommand.ExecuteNonQuery();
                        }
                        Console.WriteLine(sqliteCommand.CommandText + "\n\n"); 
                    }
                    transaction.Commit();
                    closeDatabaseConnection();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return false;
        }

        public bool HasFavoriteList(int id)
        {
            try
            {
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from favorite_list where id='" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    dataReader.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public bool DeleteFavorite(int id)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                MySqlTransaction transaction = mDBConnection.BeginTransaction();
                sqliteCommand.CommandText = "delete from favorite_info where id=\"" + id + "\";";
                sqliteCommand.ExecuteNonQuery();
                sqliteCommand.CommandText = "delete from favorite_list where favorite_info_id=\"" + id + "\";";
                sqliteCommand.ExecuteNonQuery();
                transaction.Commit();
                closeDatabaseConnection();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return false;
        }

        public FavoriteInfoDTO[] GetFavoriteInfo(int skip, int take)
        {
            try
            {
                List<FavoriteInfoDTO> favorites = new List<FavoriteInfoDTO>();
                FavoriteInfoDTO favorite = null;
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from favorite_info;";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    favorite = new FavoriteInfoDTO();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("id"))
                        {
                            favorite.Id = Convert.ToInt16(dataReader.GetValue(i));
                        }

                        if (dataReader.GetName(i).Equals("created"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                favorite.Created = (DateTime)(dataReader.GetValue(i));
                            }
                            else
                            {
                                favorite.Created = null;
                            }
                        }

                        if (dataReader.GetName(i).Equals("modified"))
                        {
                            if (dataReader.GetValue(i) != System.DBNull.Value)
                            {
                                favorite.Modified = (DateTime)(dataReader.GetValue(i));
                            }
                            else
                            {
                                favorite.Modified = null;
                            }
                        }

                        if (dataReader.GetName(i).Equals("favorite_name"))
                        {
                            favorite.FavoriteName = dataReader.GetValue(i).ToString();
                        }
                    }
                    favorites.Add(favorite);
                }
                
                closeDatabaseConnection();
                FavoriteInfoDTO[] tmpFavorite = favorites.ToArray();
                

                if (take > tmpFavorite.Length)
                {
                    take = tmpFavorite.Length;
                }

                FavoriteInfoDTO[] favoriteArray = new FavoriteInfoDTO[take];
                int j = 0;
                for (int i = skip; i < skip + take; i++)
                {
                    favoriteArray[j] = tmpFavorite[i];
                    j++;
                }

                
                for (int i = 0; i < favoriteArray.Length; i++)
                {
                    favoriteArray[i].Favorite = GetFavoriteById(favoriteArray[i].Id);
                }
                return favoriteArray;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public FavoriteListDTO[] GetFavoriteById(int id)
        {
            try
            {
                List<FavoriteListDTO> favorites = new List<FavoriteListDTO>();
                FavoriteListDTO favorite = null;
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from favorite_list where favorite_info_id=" + id + ";";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    favorite = new FavoriteListDTO();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("id"))
                        {
                            favorite.Id = Convert.ToInt16(dataReader.GetValue(i));
                        }

                        if (dataReader.GetName(i).Equals("media_id"))
                        {
                            favorite.MediaId = Convert.ToInt16(dataReader.GetValue(i));
                        }

                        if (dataReader.GetName(i).Equals("playing_mode"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 0:
                                    favorite.PlayingMode = "None";
                                    break;
                                case 1:
                                    favorite.PlayingMode = "Auto";
                                    break;
                                case 2:
                                    favorite.PlayingMode = "Manual";
                                    break;
                                case 3:
                                    favorite.PlayingMode = "Interrupt";
                                    break;
                            }
                        }

                        if (dataReader.GetName(i).Equals("sign_order"))
                        {
                            favorite.SignOrder = Convert.ToInt16(dataReader.GetValue(i));
                        }

                        if (dataReader.GetName(i).Equals("sign_type"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 0:
                                    favorite.SignType = "None";
                                    break;
                                case 1:
                                    favorite.SignType = "Ms";
                                    break;
                                case 2:
                                    favorite.SignType = "Vms";
                                    break;
                            }
                        }
                    }
                    favorites.Add(favorite);
                }
                closeDatabaseConnection();
                ImageGeneratorController imageCtrl = new ImageGeneratorController();
                FavoriteListDTO[] favoriteArray = favorites.ToArray();
                for (int i = 0; i < favoriteArray.Length; i++)
                {
                    favoriteArray[i].Image = imageCtrl.getImage(favoriteArray[i].MediaId);
                    favoriteArray[i].Image = GetImageInfo(favoriteArray[i].Image);
                }

                return favorites.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public bool SetFavoriteToPlaylist(int nodeId, int favoriteId)
        {
            try
            {
                FavoriteListDTO[] favorites = GetFavoriteById(favoriteId);
                List<MySqlCommand> sqliteCommands = new List<MySqlCommand>();

                /*MySqlTransaction transaction = mDBConnection.BeginTransaction();*/
                foreach (FavoriteListDTO favoriteList in favorites)
                {
                    int signId = GetSignId(nodeId, favoriteList.SignOrder);
                    if (HasPlaylistBySignId(signId))
                    {
                        switch (favoriteList.PlayingMode)
                        {
                            case "None":
                                favoriteList.PlayingMode = "0";
                                break;
                            case "Auto":
                                favoriteList.PlayingMode = "1";
                                break;
                            case "Manual":
                                favoriteList.PlayingMode = "2";
                                break;
                            case "Interrupt":
                                favoriteList.PlayingMode = "3";
                                break;
                        }

                        if (signId > 0)
                        {
                            openDatabaseConnection();
                            MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                            sqliteCommand.CommandText = "update playlist_info set media_id=" + favoriteList.MediaId + ", playing_mode=" + Convert.ToInt16(favoriteList.PlayingMode) + ", node_ack=0 where sign_id=\"" + signId + "\";";
                            sqliteCommands.Add(sqliteCommand);
                            closeDatabaseConnection();
                        }                 
                    }
                    else
                    {
                        openDatabaseConnection();
                        MySqlCommand sqliteCommand = mDBConnection.CreateCommand();                     
                        sqliteCommand.CommandText = "insert into playlist_info (created, node_id, sign_id, media_id, playing_mode, node_ack) values (@date, " + nodeId + ", " + signId + ", " + favoriteList.MediaId + ", " + favoriteList.PlayingMode + ", \"0\"); ";
                        MySqlParameter parameter = new MySqlParameter("@date", MySqlDbType.DateTime);
                        parameter.Value = DateTime.Now;
                        sqliteCommands.Add(sqliteCommand);
                        closeDatabaseConnection();
                        //sqliteCommand.ExecuteNonQuery();
                    }
                }

                openDatabaseConnection();
                MySqlTransaction transaction = mDBConnection.BeginTransaction();

                foreach (MySqlCommand cmd in sqliteCommands)
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
                closeDatabaseConnection();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return false;
        }

        public int GetSignId(int nodeId, int signOrder)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select sign_id from sign_info where node_id='" + nodeId + "' and sign_order='" + signOrder + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    int signId = 0;
                    if (dataReader.GetName(0).Equals("sign_id"))
                    {
                        signId = (int)dataReader.GetValue(0);
                    }
                    closeDatabaseConnection();
                    return signId;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return 0;
        }

        public bool HasPlaylistBySignId(int id)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from playlist_info where sign_id='" + id + "';";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    closeDatabaseConnection();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public ImageDTO GetImageInfo(ImageDTO image)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from media_info where id=\"" + image.Id + "\";";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (dataReader.GetName(i).Equals("media_name"))
                        {
                            image.MediaName = dataReader.GetValue(i).ToString();
                        }

                        if (dataReader.GetName(i).Equals("media_description"))
                        {
                            image.MediaDescription = dataReader.GetValue(i).ToString();
                        }

                        if (dataReader.GetName(i).Equals("media_placeholder_type"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    image.MediaPlaceholderType = "MS";
                                    break;
                                case 2:
                                    image.MediaPlaceholderType = "VMS";
                                    break;
                                default:
                                    image.MediaPlaceholderType = "Undefined";
                                    break;
                            }
                        }

                        if (dataReader.GetName(i).Equals("media_meaning_type"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    image.MediaMeaningType = "Lane Control";
                                    break;
                                case 2:
                                    image.MediaMeaningType = "Congestion";
                                    break;
                                case 3:
                                    image.MediaMeaningType = "Speed Limit";
                                    break;
                                case 4:
                                    image.MediaMeaningType = "Background";
                                    break;
                                default:
                                    image.MediaMeaningType = "Undefined";
                                    break;
                            }
                        }

                        if (dataReader.GetName(i).Equals("media_aspect_type"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    image.MediaAspectType = "Static Image";
                                    break;
                                case 2:
                                    image.MediaAspectType = "Animation Image";
                                    break;
                                case 3:
                                    image.MediaAspectType = "Image with text";
                                    break;
                                default:
                                    image.MediaAspectType = "Undefined";
                                    break;
                            }
                        }

                        if (dataReader.GetName(i).Equals("is_permanent"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    image.IsPermanent = "Unchangable";
                                    break;
                                case 2:
                                    image.IsPermanent = "changable";
                                    break;
                                default:
                                    image.IsPermanent = "Undefined";
                                    break;
                            }
                        }

                        if (dataReader.GetName(i).Equals("affair_type"))
                        {
                            switch (Convert.ToInt16(dataReader.GetValue(i)))
                            {
                                case 1:
                                    image.AfairType = "Build-in";
                                    break;
                                case 2:
                                    image.AfairType = "User";
                                    break;
                                default:
                                    image.AfairType = "All";
                                    break;
                            }
                        }
                    }
                }
                closeDatabaseConnection();
                return image;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return null;
        }

        public bool AddToGallery(MediaInfoDTO mediaInfo)
        {
            if (mediaInfo != null)
            {
                try
                {
                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    String sqlCommand = "Insert into media_info (";
                    String sqlCommand2 = ") values (";

                    sqlCommand += "created, ";
                    sqlCommand2 += "@date1, ";
                    MySqlParameter parameter = new MySqlParameter("@date1", MySqlDbType.DateTime);
                    parameter.Value = DateTime.Now;
                    sqliteCommand.Parameters.Add(parameter);


                    if (mediaInfo.ModifiedBy != 0)
                    {
                        sqlCommand += "modified_by, ";
                        sqlCommand2 += "'" + mediaInfo.ModifiedBy + "', ";
                    }

                    if (mediaInfo.MediaName != null && !mediaInfo.MediaName.Equals(""))
                    {
                        sqlCommand += "media_name, ";
                        sqlCommand2 += "'" + mediaInfo.MediaName + "', ";
                    }

                    if (mediaInfo.MediaDescription != null && !mediaInfo.MediaDescription.Equals(""))
                    {
                        sqlCommand += "media_description, ";
                        sqlCommand2 += "'" + mediaInfo.MediaDescription + "', ";
                    }

                    if (mediaInfo.MediaPlaceholderType != null && !mediaInfo.MediaPlaceholderType.Equals(""))
                    {
                        sqlCommand += "media_placeholder_type, ";
                        sqlCommand2 += "'" + mediaInfo.MediaPlaceholderType + "', ";
                    }

                    if (mediaInfo.MediaMeaningType != null && !mediaInfo.MediaMeaningType.Equals(""))
                    {
                        sqlCommand += "media_meaning_type, ";
                        sqlCommand2 += "'" + mediaInfo.MediaMeaningType + "', ";
                    }

                    if (mediaInfo.MediaAspectType != null && !mediaInfo.MediaAspectType.Equals(""))
                    {
                        sqlCommand += "media_aspect_type, ";
                        sqlCommand2 += "'" + mediaInfo.MediaAspectType + "', ";
                    }

                    if (mediaInfo.Image != null)
                    {
                        Image tmpImage = Base64ToImage(mediaInfo.Image);
                        string MediaPlaceholderType = "";
                        switch (mediaInfo.MediaPlaceholderType)
                        {
                            case 1:
                                MediaPlaceholderType = "MS";
                                break;
                            case 2:
                                MediaPlaceholderType = "VMS";
                                break;
                            default:
                                MediaPlaceholderType = "Undefined";
                                break;
                        }
                        mediaInfo.Image = "{\"Width\":" + tmpImage.Width + ",\"Height\":" + tmpImage.Height + ",\"SignType\":\"" + MediaPlaceholderType + "\",\"Frame\":[{\"DelayTime\":1,\"BackgroundColor\":\"black\",\"Element\":[{\"ContentType\":\"image\",\"TextSize\":10,\"TextColor\":\"\",\"Text\":\"\",\"Image\":\"" + mediaInfo.Image + "\",\"ElementWidth\":" + tmpImage.Width + ",\"ElementHeight\":" + tmpImage.Height + ",\"StartPointX\":0,\"StartPointY\":0,\"FloatingLevel\":0,\"IsEditable\":false,\"Reference\":\"\"}]}]}";
                    }

                    if (mediaInfo.Image != null)
                    {
                        sqlCommand += "media_data, ";
                        sqlCommand2 += "'" + mediaInfo.Image + "', ";
                    }

                    if (mediaInfo.Image != null)
                    {
                        sqlCommand += "affair_type, ";
                        sqlCommand2 += "'" + mediaInfo.MediaAffairType + "', ";
                    }

                    if (mediaInfo.IsPermanent != null && !mediaInfo.IsPermanent.Equals(""))
                    {
                        sqlCommand += "is_permanent";
                        sqlCommand2 += "'" + mediaInfo.IsPermanent + "'";
                    }

                    sqlCommand += sqlCommand2 + ");";
                    sqliteCommand.CommandText = sqlCommand;
                    sqliteCommand.ExecuteNonQuery();
                    closeDatabaseConnection();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return false;
        }

        public bool EditGallery(MediaInfoDTO mediaInfo)
        {
            if (mediaInfo != null)
            {
                try
                {
                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    String sqlCommand = "update media_info set ";

                    if (mediaInfo.ModifiedBy != 0)
                    {
                        sqlCommand += "modified_by = ";
                        sqlCommand += "'" + mediaInfo.ModifiedBy + "', ";
                    }

                    if (mediaInfo.MediaName != null && !mediaInfo.MediaName.Equals(""))
                    {
                        sqlCommand += "media_name = ";
                        sqlCommand += "'" + mediaInfo.MediaName + "', ";
                    }

                    if (mediaInfo.MediaDescription != null && !mediaInfo.MediaDescription.Equals(""))
                    {
                        sqlCommand += "media_description = ";
                        sqlCommand += "'" + mediaInfo.MediaDescription + "', ";
                    }

                    if (mediaInfo.MediaPlaceholderType != null && !mediaInfo.MediaPlaceholderType.Equals(""))
                    {
                        sqlCommand += "media_placeholder_type = ";
                        sqlCommand += "'" + mediaInfo.MediaPlaceholderType + "', ";
                    }

                    if (mediaInfo.MediaMeaningType != null && !mediaInfo.MediaMeaningType.Equals(""))
                    {
                        sqlCommand += "media_meaning_type = ";
                        sqlCommand += "'" + mediaInfo.MediaMeaningType + "', ";
                    }

                    if (mediaInfo.MediaAspectType != null && !mediaInfo.MediaAspectType.Equals(""))
                    {
                        sqlCommand += "media_aspect_type = ";
                        sqlCommand += "'" + mediaInfo.MediaAspectType + "', ";
                    }

                    if (mediaInfo.Image != null)
                    {
                        Image tmpImage = Base64ToImage(mediaInfo.Image);
                        string MediaPlaceholderType = "";
                        switch (mediaInfo.MediaPlaceholderType)
                        {
                            case 1:
                                MediaPlaceholderType = "MS";
                                break;
                            case 2:
                                MediaPlaceholderType = "VMS";
                                break;
                            default:
                                MediaPlaceholderType = "Undefined";
                                break;
                        }
                        mediaInfo.Image = "{\"Width\":" + tmpImage.Width + ",\"Height\":" + tmpImage.Height + ",\"SignType\":\"" + MediaPlaceholderType + "\",\"Frame\":[{\"DelayTime\":5,\"BackgroundColor\":\"black\",\"Element\":[{\"ContentType\":\"image\",\"TextSize\":10,\"TextColor\":\"\",\"Text\":\"\",\"Image\":\"" + mediaInfo.Image + "\",\"ElementWidth\":" + tmpImage.Width + ",\"ElementHeight\":" + tmpImage.Height + ",\"StartPointX\":0,\"StartPointY\":0,\"FloatingLevel\":0,\"IsEditable\":false,\"Reference\":\"\"}]}]}";
                    }

                    if (mediaInfo.Image != null)
                    {
                        sqlCommand += "media_data = ";
                        sqlCommand += "'" + mediaInfo.Image + "', ";
                    }

                    if (mediaInfo.Image != null)
                    {
                        sqlCommand += "affair_type = ";
                        sqlCommand += "'" + mediaInfo.MediaAffairType + "', ";
                    }

                    if (mediaInfo.IsPermanent != null && !mediaInfo.IsPermanent.Equals(""))
                    {
                        sqlCommand += "is_permanent = ";
                        sqlCommand += "'" + mediaInfo.IsPermanent + "'";
                    }

                    sqlCommand += " where id = '" + mediaInfo.Id + "';";
                    sqliteCommand.CommandText = sqlCommand;
                    sqliteCommand.ExecuteNonQuery();
                    closeDatabaseConnection();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return false;
        }

        public ResponseMessageDTO DeleteGallery(int id, int forcedelete)
        {
            //forcedelete   0 = not force, 1 = force
            ResponseMessageDTO Response = new ResponseMessageDTO();
            try
            {
                if (forcedelete == 0)
                {
                    if (!IsMediaPlaying(id))
                    {
                        openDatabaseConnection();
                        MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                        sqliteCommand.CommandText = "delete from media_info where id = " + id + ";";
                        sqliteCommand.ExecuteNonQuery();
                        closeDatabaseConnection();

                        Response.Code = 1;
                        Response.Message = "Success";
                        Response.Description = "";
                    }
                    else
                    {
                        Response.Code = 0;
                        Response.Message = "Not success";
                        Response.Description = "Media กำลังเล่นบนป้าย";
                    }
                }
                else
                {
                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    sqliteCommand.CommandText = "delete from playlist_info where media_id = " + id + ";";
                    sqliteCommand.ExecuteNonQuery();
                    sqliteCommand.CommandText = "delete from media_info where id = " + id + ";";
                    sqliteCommand.ExecuteNonQuery();
                    closeDatabaseConnection();

                    Response.Code = 1;
                    Response.Message = "Success";
                    Response.Description = "";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            closeDatabaseConnection();
            return Response;
        }

        public bool IsMediaPlaying(int id)
        {
            try
            {
                openDatabaseConnection();
                MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                sqliteCommand.CommandText = "select * from playlist_info where media_id=\"" + id + "\";";
                MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    closeDatabaseConnection();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            closeDatabaseConnection();
            return false;
        }

        public bool AddSchedule(ScheduleListInfoDTO ScheduleListInfo)
        {
            if (ScheduleListInfo != null)
            {

                List<MySqlCommand> MySqlCommands = new List<MySqlCommand>();

                try
                {
                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    String sqlCommand = "Insert into schedule_list_info (";
                    String sqlCommand2 = ") values (";

                    sqlCommand += "created, ";
                    sqlCommand2 += "@date1, ";
                    MySqlParameter parameter = new MySqlParameter("@date1", MySqlDbType.DateTime);
                    parameter.Value = DateTime.Now;
                    sqliteCommand.Parameters.Add(parameter);


                    if (ScheduleListInfo.ModifiedBy != 0)
                    {
                        sqlCommand += "modified_by, ";
                        sqlCommand2 += "'" + ScheduleListInfo.ModifiedBy + "', ";
                    }

                    if (ScheduleListInfo.Description != null && !ScheduleListInfo.Description.Equals(""))
                    {
                        sqlCommand += "description";
                        sqlCommand2 += "'" + ScheduleListInfo.Description + "'";
                    }

                    sqlCommand += sqlCommand2 + ");";
                    sqliteCommand.CommandText = sqlCommand;
                    sqliteCommand.ExecuteNonQuery();

                    sqliteCommand = mDBConnection.CreateCommand();
                    sqliteCommand.CommandText = "select * from schedule_list_info order by schedule_list_info_id desc;";
                    MySqlDataReader dataReader = sqliteCommand.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            if (dataReader.GetName(i).Equals("schedule_list_info_id"))
                            {
                                ScheduleListInfo.ScheduleDate.ScheduleListInfoId = (int)dataReader.GetValue(i);
                            }
                        }
                    }
                    dataReader.Close();

                    Console.WriteLine(ScheduleListInfo.ScheduleDate.ScheduleListInfoId + "\n\n");
                    sqliteCommand = mDBConnection.CreateCommand();

                    if (ScheduleListInfo.ScheduleDate != null)
                    {
                        sqlCommand = "Insert into schedule_date (";
                        sqlCommand2 = ") values (";

                        sqlCommand += "created";
                        sqlCommand2 += "@date1";
                        parameter = new MySqlParameter("@date1", MySqlDbType.DateTime);
                        parameter.Value = DateTime.Now;
                        sqliteCommand.Parameters.Add(parameter);

                        if (ScheduleListInfo.ScheduleDate.NodeId > 0)
                        {
                            sqlCommand += ", node_id";
                            sqlCommand2 += ", '" + ScheduleListInfo.ScheduleDate.NodeId + "'";
                        }

                        if (ScheduleListInfo.ScheduleDate.ScheduleDate != null)
                        {
                            sqlCommand += ", schedule_date";
                            sqlCommand2 += ", @date2";
                            parameter = new MySqlParameter("@date2", MySqlDbType.DateTime);
                            parameter.Value = ScheduleListInfo.ScheduleDate.ScheduleDate;
                        }

                        if (ScheduleListInfo.ScheduleDate.ScheduleListInfoId > 0)
                        {
                            sqlCommand += ", schedule_list_info_id";
                            sqlCommand2 += ", '" + ScheduleListInfo.ScheduleDate.ScheduleListInfoId + "'";
                        }

                        sqlCommand += sqlCommand2 + ");";
                        sqliteCommand.CommandText = sqlCommand;

                        Console.WriteLine(sqliteCommand.CommandText + "\n\n");
                        sqliteCommand.ExecuteNonQuery();

                        sqliteCommand = mDBConnection.CreateCommand();
                        foreach (ScheduleListDTO ScheduleList in ScheduleListInfo.ScheduleDate.ScheduleList)
                        {
                            sqlCommand = "Insert into schedule_list (";
                            sqlCommand2 = ") values (";

                            sqlCommand += "created";
                            sqlCommand2 += "@date1";
                            parameter = new MySqlParameter("@date1", MySqlDbType.DateTime);
                            parameter.Value = DateTime.Now;
                            sqliteCommand.Parameters.Add(parameter);

                            if (ScheduleList.StartTime != null)
                            {
                                sqlCommand += ", start_time";
                                sqlCommand2 += ", @time1";
                                parameter = new MySqlParameter("@time1", MySqlDbType.Time);
                                parameter.Value = ScheduleList.StartTime;
                                sqliteCommand.Parameters.Add(parameter);
                            }

                            if (ScheduleList.EndTime != null)
                            {
                                sqlCommand += ", end_time";
                                sqlCommand2 += ", @time2";
                                parameter = new MySqlParameter("@time2", MySqlDbType.Time);
                                parameter.Value = ScheduleList.EndTime;
                                sqliteCommand.Parameters.Add(parameter);
                            }

                            if (ScheduleList.SignId > 0)
                            {
                                sqlCommand += ", sign_id";
                                sqlCommand2 += ", '" + ScheduleList.SignId + "'";
                            }

                            if (ScheduleList.MediaId > 0)
                            {
                                sqlCommand += ", media_id";
                                sqlCommand2 += ", '" + ScheduleList.MediaId + "'";
                            }

                            if (ScheduleListInfo.ScheduleDate.ScheduleListInfoId > 0)
                            {
                                sqlCommand += ", schedule_list_info_id";
                                sqlCommand2 += ", '" + ScheduleListInfo.ScheduleDate.ScheduleListInfoId + "'";
                            }

                            sqlCommand += sqlCommand2 + ");";
                            sqliteCommand.CommandText = sqlCommand;
                            sqliteCommand.ExecuteNonQuery();
                        }
                    }
                    sqliteCommand.ExecuteNonQuery();
                    closeDatabaseConnection();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return false;
        }

        public bool EditSchedule(ScheduleListInfoDTO ScheduleListInfo)
        {
            if (ScheduleListInfo != null)
            {
                try
                {
                    String sqlCommand = "update schedule_list_info set";

                    if (ScheduleListInfo.Description != null && !ScheduleListInfo.Description.Equals(""))
                    {
                        sqlCommand += " description='" + ScheduleListInfo.Description + "'";
                    }

                    if (ScheduleListInfo.ModifiedBy > 0)
                    {
                        sqlCommand += ", modified_by='" + ScheduleListInfo.ModifiedBy + "'";
                    }

                    sqlCommand += " where schedule_list_info_id='" + ScheduleListInfo.Id + "';";

                    openDatabaseConnection();
                    MySqlCommand sqliteCommand = mDBConnection.CreateCommand();
                    sqliteCommand.CommandText = sqlCommand;
                    sqliteCommand.ExecuteNonQuery();

                    sqliteCommand = mDBConnection.CreateCommand();

                    if (ScheduleListInfo.ScheduleDate != null)
                    {
                        sqlCommand = "update schedule_date set ";
                        if (ScheduleListInfo.ScheduleDate.NodeId > 0)
                        {
                            sqlCommand += " node_id='" + ScheduleListInfo.ScheduleDate.NodeId + "'";
                        }

                        if (ScheduleListInfo.ScheduleDate.ScheduleDate != null)
                        {
                            sqlCommand += ", schedule_date=@date";
                            MySqlParameter parameter = new MySqlParameter("@date", MySqlDbType.DateTime);
                            parameter.Value = ScheduleListInfo.ScheduleDate.ScheduleDate;
                            sqliteCommand.Parameters.Add(parameter);
                        }

                        sqlCommand += " where id='" + ScheduleListInfo.ScheduleDate.Id + "';";
                    }

                    sqliteCommand.CommandText = sqlCommand;
                    sqliteCommand.ExecuteNonQuery();


                    if (ScheduleListInfo.ScheduleDate.ScheduleList != null)
                    {
                        foreach (ScheduleListDTO ScheduleList in ScheduleListInfo.ScheduleDate.ScheduleList)
                        {
                            sqliteCommand = mDBConnection.CreateCommand();
                            sqlCommand = "update schedule_list set ";
                            if (ScheduleList.MediaId > 0)
                            {
                                sqlCommand += " media_id='" + ScheduleList.MediaId + "'";
                            }

                            if (ScheduleList.SignId > 0)
                            {
                                sqlCommand += ", sign_id='" + ScheduleList.SignId + "'";
                            }

                            if (ScheduleList.StartTime != null)
                            {
                                sqlCommand += ", start_time=@time1";
                                MySqlParameter parameter = new MySqlParameter("@time1", MySqlDbType.Time);
                                parameter.Value = ScheduleList.StartTime;
                                sqliteCommand.Parameters.Add(parameter);
                            }

                            if (ScheduleList.EndTime != null)
                            {
                                sqlCommand += ", end_time=@time2";
                                MySqlParameter parameter = new MySqlParameter("@time2", MySqlDbType.Time);
                                parameter.Value = ScheduleList.EndTime;
                                sqliteCommand.Parameters.Add(parameter);
                            }

                            sqlCommand += " where id='" + ScheduleList.Id + "';";
                            sqliteCommand.CommandText = sqlCommand;
                            sqliteCommand.ExecuteNonQuery();
                        }
                    }
                    closeDatabaseConnection();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return false;
        }

        private Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }
    }
}
