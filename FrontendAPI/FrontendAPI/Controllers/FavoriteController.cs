﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/favorite")]
    public class FavoriteController : ApiController
    {
        private DBHelper mDBHelper = new DBHelper();

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage AddToFavorite(FavoriteInfoDTO favorite)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.AddToFavorite(favorite))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }    
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("edit")]
        [HttpPost]
        public HttpResponseMessage EditFavorite(FavoriteInfoDTO favorite)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.EditFavorite(favorite))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("delete/{id:int}")]
        [HttpGet]
        public HttpResponseMessage DeleteFavorite(int id)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.DeleteFavorite(id))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("set/{nodeid:int}/{id:int}")]
        [HttpGet]
        public HttpResponseMessage SetFavorite(int nodeid,int id)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.SetFavoriteToPlaylist(nodeid,id))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("get/{skip:int}/{take:int}")]
        [HttpGet]
        public HttpResponseMessage GetFavorite(int skip, int take)
        {
            try
            {
                FavoriteInfoDTO[] favorite = mDBHelper.GetFavoriteInfo(skip,take);
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, favorite);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.NoContent);
        }
    }
}
