﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/changesigndisplay")]
    public class ChangeSignDisplayController : ApiController
    {

        DBHelper mDBHelper = new DBHelper();

        [Route("changedisplay/{signid:int}/{imageid:int}")]
        [HttpGet]
        public HttpResponseMessage changedisplay(int signid, int imageid)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if(mDBHelper.ChangeSignDisplay(signid, imageid))
                { 
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }
    }
}
