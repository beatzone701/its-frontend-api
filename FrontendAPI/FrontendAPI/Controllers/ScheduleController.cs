﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/schedule")]
    public class ScheduleController : ApiController
    {
        private DBHelper mDBHelper = new DBHelper();

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage AddToSchedule(ScheduleListInfoDTO ScheduleListInfo)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.AddSchedule(ScheduleListInfo))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("edit")]
        [HttpPost]
        public HttpResponseMessage EditSchedule(ScheduleListInfoDTO ScheduleListInfo)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.EditSchedule(ScheduleListInfo))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("delete/{id:int}")]
        [HttpGet]
        public HttpResponseMessage DeleteSchedule(int id)
        {
            ChangeDisplayResponseDTO Response = new ChangeDisplayResponseDTO();
            try
            {
                if (mDBHelper.DeleteSchedule(id))
                {
                    Response.Code = 1;
                    Response.Message = "Success";
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Response.Code = 0;
            Response.Message = "Not success";
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }
    }
}
