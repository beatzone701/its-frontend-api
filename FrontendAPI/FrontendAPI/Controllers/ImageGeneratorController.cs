﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Drawing;
using System.Web.Script.Serialization;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using Gif.Components;
using FrontendAPI.Models;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/getallimage")]
    public class ImageGeneratorController : ApiController
    {

        private DBHelper mDBHelper = new DBHelper();
        private PlaylistDTO playlist;

        [Route("getimage/{mediatype:int}/{affairtype:int}/{skip:int}/{take:int}")]
        [HttpGet]
        public HttpResponseMessage GetImage(int mediatype, int affairtype, int skip, int take)
        {
            try
            {
                int[] mediaId = mDBHelper.GetMediaId(mediatype, affairtype, skip, take);
                List<ImageDTO> imageList = new List<ImageDTO>();
                ImageDTO image;
                foreach (int i in mediaId)
                {
                    if ((image = getImage(i)) != null)
                    {
                        image = mDBHelper.GetImageInfo(image);
                        imageList.Add(image);
                    }
                }
                if (imageList.Count > 0)
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, imageList.ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.NoContent);
        }

        public ImageDTO getImage(int id)
        {
            try
            {
                //Console.WriteLine(DateTime.Now + " > Send image.");
                string data = mDBHelper.RetrieveImageData(id);
                //Console.WriteLine(data);
                if (data != null && !data.Equals(""))
                {
                    Image[] image = CreateImage(data);

                    if (image != null)
                    {
                        if (image.Count() > 1)
                        {
                            List<int> delay = new List<int>();
                            for (int i = 0; i < playlist.Frame.Count(); i++)
                            {
                                delay.Add(playlist.Frame[i].DelayTime);
                            }

                            //CreateGifFile(image, delay.ToArray(), id);
                            //SendResponseImage(image, delay.ToArray(), id);
                            //return Request.CreateResponse(System.Net.HttpStatusCode.OK, "localhost:" + Program.serverPort + @"/userimages/" + id + ".gif");
                            ImageDTO imageResponse;
                            if ((imageResponse = SendResponseImage(image, delay.ToArray(), id)) != null)
                                return SendResponseImage(image, delay.ToArray(), id);
                        }
                        else
                        {
                            //SaveImageToFile(image[0], id);                    
                            //return Request.CreateResponse(System.Net.HttpStatusCode.OK, "localhost:" + Program.serverPort + @"/userimages/" + id + ".jpg");
                            ImageDTO imageResponse;
                            if ((imageResponse = SendResponseImage(image[0], id)) != null)
                                return SendResponseImage(image[0], id);
                        }

                    }
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        private Image[] CreateImage(string data)
        {
            try
            {
                playlist = (new JavaScriptSerializer()).Deserialize<PlaylistDTO>(data);
                List<Image> imageList = new List<Image>();
                for (int frameNumber = 0; frameNumber < playlist.Frame.Count(); frameNumber++)
                {
                    // Create a bitmap to get graphic object
                    Image image = new Bitmap(playlist.Width, playlist.Height);
                    Graphics drawing = Graphics.FromImage(image);

                    // Create Color, font and background
                    Color backColor = System.Drawing.Color.FromName(playlist.Frame[frameNumber].BackgroundColor);
                    Color textColor = System.Drawing.Color.FromName(playlist.Frame[frameNumber].Element[0].TextColor);

                    // Paint the background
                    drawing.Clear(backColor);

                    // Create a brush tor the text
                    Brush textBrush = new SolidBrush(textColor);

                    // Create font
                    PrivateFontCollection privateFontCollection = new PrivateFontCollection();
                    privateFontCollection.AddFontFile(Path.GetDirectoryName(Application.ExecutablePath) + @"\SarunsThangLuang_3.ttf");
                    FontFamily fontFamily = new FontFamily(((FontFamily)privateFontCollection.Families.GetValue(0)).Name, privateFontCollection);
                    Font font = new Font(fontFamily, playlist.Frame[frameNumber].Element[0].TextSize, FontStyle.Regular);

                    // Draw and save
                    for (int floatingLevel = 0; floatingLevel < 10; floatingLevel++)
                    {
                        for (int i = 0; i < playlist.Frame[frameNumber].Element.Count(); i++)
                        {
                            if (playlist.Frame[frameNumber].Element[i].FloatingLevel == floatingLevel)
                            {
                                RectangleF drawRect = new RectangleF(playlist.Frame[frameNumber].Element[i].StartPointX, playlist.Frame[frameNumber].Element[i].StartPointY, playlist.Frame[frameNumber].Element[i].ElementWidth, playlist.Frame[frameNumber].Element[i].ElementHeight);
                                if (playlist.Frame[frameNumber].Element[i].ContentType.Equals("text"))
                                {
                                    StringFormat drawFormat = new StringFormat();
                                    //drawFormat.LineAlignment = StringAlignment.Center;
                                    drawing.DrawString(playlist.Frame[frameNumber].Element[i].Text, font, textBrush, drawRect, drawFormat);
                                    //Rectangle roundedRectangle = Rectangle.Round(drawRect);
                                    //Pen redPen = new Pen(Color.Blue, 4);
                                    //drawing.DrawRectangle(redPen, roundedRectangle);
                                }
                                else
                                {
                                    var ms = new MemoryStream(StringToByte((playlist.Frame[frameNumber].Element[i].Image)));
                                    drawing.DrawImage(Image.FromStream(ms, true), drawRect);
                                    //Rectangle roundedRectangle = Rectangle.Round(drawRect);
                                    //Pen redPen = new Pen(Color.Blue, 4);
                                    //drawing.DrawRectangle(redPen, roundedRectangle);
                                }
                            }
                        }
                    }

                    drawing.Save();

                    // Free up
                    textBrush.Dispose();
                    drawing.Dispose();

                    imageList.Add(image);
                }

                return imageList.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " > " + ex.ToString() + "\n");
            }

            return null;
        }

        public void SaveImageToFile(Image image, int id)
        {
            try
            {
                image.Save(@".\userimages\" + id.ToString() + ".PNG", ImageFormat.Png);
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " > " + ex.ToString() + "\n");
            }
        }

        public ImageDTO SendResponseImage(Image image, int id)
        {
            try
            {
                ImageDTO ResponseImage = new ImageDTO();
                ResponseImage.Extension = "PNG";
                ResponseImage.Id = id;
                ResponseImage.Image = Convert.ToBase64String(ImageToByte(image));
                return ResponseImage;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public ImageDTO SendResponseImage(Image[] image, int[] delay, int id)
        {
            try
            {
                AnimatedGifEncoder animatedGifEncoder = new AnimatedGifEncoder();
                animatedGifEncoder.Start(@".\userimages\" + id.ToString() + ".gif");
                animatedGifEncoder.SetRepeat(0);
                for (int i = 0; i < image.Count(); i++)
                {
                    animatedGifEncoder.SetDelay((delay[i] * 1000));
                    animatedGifEncoder.AddFrame(image[i]);
                }              
                animatedGifEncoder.Finish();

                //byte[] bytes = File.ReadAllBytes(@".\userimages\" + id.ToString() + ".gif");            
                //MemoryStream imageMemoryStream = new MemoryStream(File.ReadAllBytes(@".\userimages\" + id.ToString() + ".gif"), false);
                
                FileStream imageFile = new FileStream(@".\userimages\" + id.ToString() + ".gif", FileMode.Open, FileAccess.Read, FileShare.Read);
                byte[] bytes = new byte[imageFile.Length];
                imageFile.Read(bytes, 0, (int)imageFile.Length);
                //imageMemoryStream.Read(bytes, 0, (int)imageMemoryStream.Length);
                //imageMemoryStream.Close();
                ImageDTO ResponseImage = new ImageDTO();
                ResponseImage.Extension = "gif";
                ResponseImage.Id = id;
                ResponseImage.Image = Convert.ToBase64String(bytes);
                return ResponseImage;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public void CreateGifFile(Image[] image, int[] delay, int id)
        {
            try
            {
                AnimatedGifEncoder animatedGifEncoder = new AnimatedGifEncoder();
                animatedGifEncoder.Start(@".\userimages\" + id.ToString() + ".gif");
                animatedGifEncoder.SetRepeat(0);
                for (int i = 0; i < image.Count(); i++)
                {
                    animatedGifEncoder.SetDelay(delay[i]);
                    animatedGifEncoder.AddFrame(image[i]);
                }
                animatedGifEncoder.Finish();
            }
            catch(Exception ex)
            {
                Console.WriteLine(DateTime.Now + " > " + ex.ToString() + "\n");
            }
        }

        public byte[] StringToByte(string image)
        {
            return Convert.FromBase64String(image);
        }

        public byte[] ImageToByte(Image image)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(image, typeof(byte[]));
        }
    }
}
