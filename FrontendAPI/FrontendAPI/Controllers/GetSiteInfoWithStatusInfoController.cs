﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/sitewithstatusinfo")]
    public class GetSiteInfoWithStatusInfoController : ApiController
    {

        DBHelper mDBHelper = new DBHelper();

        [Route("get/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetSiteInfo(int id)
        {
            SiteInfoWithStatusInfoDTO data;
            if (id == 0)
            {
                List<SiteInfoWithStatusInfoDTO> SiteInfoWithStatusInfoList = new List<SiteInfoWithStatusInfoDTO>();
                SiteNameDTO[] SiteName;
                if ((SiteName = mDBHelper.GetAllSiteInfo()) != null)
                {
                    foreach (SiteNameDTO temp in SiteName)
                    {
                        if ((data = mDBHelper.GetSiteInfoWithStatusInfoBySiteId(temp.SiteID)) != null)
                        {
                            SiteInfoWithStatusInfoList.Add(data);
                        }
                    }
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, SiteInfoWithStatusInfoList);
                }
            }

            if ((data = mDBHelper.GetSiteInfoWithStatusInfoBySiteId(id)) != null)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.NoContent);
        }
    }
}
