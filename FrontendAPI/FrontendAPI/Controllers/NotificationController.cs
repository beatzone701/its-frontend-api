﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/notification")]
    public class NotificationController : ApiController
    {
        private DBHelper mDBHelper = new DBHelper();

        [Route("get/{id:int}/{top:int}")]
        [HttpGet]
        public HttpResponseMessage GetAllNotiInfo(int id, int top)
        {

            if (id == 0)
            {
                SiteNameDTO[] siteNameList;

                List<NotificationListDTO> AllNoti = new List<NotificationListDTO>();
                if ((siteNameList = mDBHelper.GetAllSiteInfo()) != null)
                {
                    foreach (SiteNameDTO site in siteNameList)
                    {
                        NotificationListDTO notificationList = new NotificationListDTO();
                        if (mDBHelper.HasNoti(site.SiteID.ToString()))
                        {
                            notificationList.SiteName = site;
                            if (top > 0)
                            {
                                notificationList.Notification = mDBHelper.GetNotification(site.SiteID.ToString(), top);
                                top -= notificationList.Notification.Count();
                                AllNoti.Add(notificationList);
                            }    
                        }
                    }
                }

                if (AllNoti.Count > 0)
                {
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, AllNoti);
                }
            }
            else
            {
                SiteNameDTO siteName = mDBHelper.GetSiteInfoById(id);
                NotificationListDTO notificationList = new NotificationListDTO();
                if (mDBHelper.HasNoti(id.ToString()))
                {
                    notificationList.SiteName = siteName;
                    if (top > 0)
                    {
                        notificationList.Notification = mDBHelper.GetNotification(id.ToString(), top);
                    }
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK, notificationList);
                }
            }     
            return Request.CreateResponse(System.Net.HttpStatusCode.NoContent);
        }
    }
}
