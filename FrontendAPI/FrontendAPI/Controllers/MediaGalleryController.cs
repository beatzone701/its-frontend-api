﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/gallery")]
    public class MediaGalleryController : ApiController
    {

        private DBHelper mDBHelper = new DBHelper();

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage AddToGallery(MediaInfoDTO mediaInfo)
        {
            //Check 1. Image ratio.
            //      2. Image size <= 30 kb

            ResponseMessageDTO Response = CheckImage(Base64ToImage(mediaInfo.Image), mediaInfo.MediaPlaceholderType);

            if (Response.Code == 1)
            {
                if (!mDBHelper.AddToGallery(mediaInfo))
                {
                    Response.Code = 0;
                    Response.Message = "Not success";
                    Response.Description = "ไม่สามารถบันทึกลงฐานข้อมูลได้";
                }
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("edit")]
        [HttpPost]
        public HttpResponseMessage EditToGallery(MediaInfoDTO mediaInfo)
        {
            //Check 1. Image ratio.
            //      2. Image size <= 30 kb

            ResponseMessageDTO Response = CheckImage(Base64ToImage(mediaInfo.Image), mediaInfo.MediaPlaceholderType);

            if (Response.Code == 1)
            {
                if (!mDBHelper.EditGallery(mediaInfo))
                {
                    Response.Code = 0;
                    Response.Message = "Not success";
                    Response.Description = "ไม่สามารถบันทึกลงฐานข้อมูลได้";
                }
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        [Route("delete/{id:int}/{forcedelete:int}")]
        [HttpGet]
        public HttpResponseMessage DeleteGallery(int id, int forcedelete)
        {
            ResponseMessageDTO Response = mDBHelper.DeleteGallery(id, forcedelete);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, Response);
        }

        private ResponseMessageDTO CheckImage(Image image, int MediaPlaceholderType)
        {
            ResponseMessageDTO Response = new ResponseMessageDTO();
            Response.Code = 1;
            Response.Message = "Success";
            Response.Description = "";

            if ((image.Width / image.Height) != 1f && (image.Width / image.Height) != 2.5f)
            {
                Response.Code = 0;
                Response.Message = "Not success";
                Response.Description += "อัตราส่วนของรูปภาพไม่ถูกต้อง";
            }

            image.Save(@".\userimages\tmpImage.PNG", ImageFormat.Png);

            if (MediaPlaceholderType == 1 && (new FileInfo(@".\userimages\tmpImage.PNG")).Length > (100.0f * 1024f))
            {                
                if (Response.Code != 0 || Response.Equals(""))
                {
                    Response.Code = 0;
                    Response.Message = "Not success";
                    Response.Description += "ขนาดของรูปภาพใหญ่เกินไป";
                }
                else
                {
                    Response.Code = 0;
                    Response.Message = "Not success";
                    Response.Description += "\nขนาดของรูปภาพใหญ่เกินไป";
                }
            }
            else if (MediaPlaceholderType == 2 && (new FileInfo(@".\userimages\tmpImage.PNG")).Length > (600.0f * 1024f))
            {
                if (Response.Code != 0 || Response.Equals(""))
                {
                    Response.Code = 0;
                    Response.Message = "Not success";
                    Response.Description += "ขนาดของรูปภาพใหญ่เกินไป";
                }
                else
                {
                    Response.Code = 0;
                    Response.Message = "Not success";
                    Response.Description += "\nขนาดของรูปภาพใหญ่เกินไป";
                }
            }

            //Response.Description += new FileInfo(@".\userimages\tmpImage.PNG").Length;

            return Response;
        }

        private Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }
    }
}
