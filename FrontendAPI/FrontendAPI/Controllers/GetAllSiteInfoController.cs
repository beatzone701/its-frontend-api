﻿using FrontendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI.Controllers
{
    [RoutePrefix("api/allsiteinfo")]
    public class GetAllSiteInfoController : ApiController
    {

        DBHelper mDBHelper = new DBHelper();

        [Route("get")]
        [HttpGet]
        public HttpResponseMessage GetAllSiteInfo()
        {
            SiteNameDTO[] data;
            if ((data = mDBHelper.GetAllSiteInfo()) != null)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.NoContent);
        }
    }
}
