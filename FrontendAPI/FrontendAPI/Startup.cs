﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FrontendAPI
{
    class Startup
    {
        // This method is required by Katana:
        public void Configuration(IAppBuilder app)
        {
            var webApiConfiguration = ConfigureWebApi();

            // Use the extension method provided by the WebApi.Owin library:
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(webApiConfiguration);
            
        }

        private HttpConfiguration ConfigureWebApi()
        {
            var config = new HttpConfiguration();

            // Template routing
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}");

            //  Enable attribute based routing
            config.MapHttpAttributeRoutes();

            return config;
        }
    }
}
